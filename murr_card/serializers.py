from django.conf import settings
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from rest_framework import serializers
from taggit_serializer.serializers import (
    TagListSerializerField,
    TaggitSerializer
)

from .models import MurrCard, EditorImageForMurrCard, Category


class MurrCardCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class MurrCardSerializers(
    EnumChoiceModelSerializerMixin,
    TaggitSerializer,
    serializers.ModelSerializer
):
    owner_name = serializers.ReadOnlyField(source='owner.username')
    owner_avatar = serializers.SerializerMethodField()
    likes = serializers.ReadOnlyField()
    dislikes = serializers.ReadOnlyField()
    comments_count = serializers.ReadOnlyField()
    tags = TagListSerializerField(required=False)
    category = serializers.SlugRelatedField(allow_null=True, queryset=Category.objects.all(), required=False,
                                            slug_field='slug')
    category_name = serializers.ReadOnlyField(source='category.name')
    is_like = serializers.SerializerMethodField()
    is_dislike = serializers.SerializerMethodField()
    is_subscriber = serializers.SerializerMethodField()

    class Meta:
        model = MurrCard
        fields = (
            'id',
            'title',
            'cover',
            'content',
            'rating',
            'timestamp',
            'owner',
            'owner_name',
            'owner_avatar',
            'status',
            'likes',
            'dislikes',
            'comments_count',
            'tags',
            'category',
            'category_name',
            'is_like',
            'is_dislike',
            'is_subscriber',
        )
        read_only_fields = ('rating', 'timestamp')

    @property
    def user(self):
        request = self.context.get('request')
        return request.user if request else None

    def get_owner_avatar(self, murr_card):
        if murr_card.owner.murren_avatar and hasattr(murr_card.owner.murren_avatar, 'url'):
            return f'{settings.BACKEND_URL}{murr_card.owner.murren_avatar.url}'

    def get_is_like(self, obj) -> bool:
        return obj.liked_murrens.filter(id=self.user.id).exists() if self.user else False

    def get_is_dislike(self, obj) -> bool:
        return obj.disliked_murrens.filter(id=self.user.id).exists() if self.user else False

    def get_is_subscriber(self, obj) -> bool:
        return obj.owner.in_subscribers(self.user) if self.user else False


class EditorImageForMurrCardSerializers(serializers.ModelSerializer):
    class Meta:
        model = EditorImageForMurrCard
        fields = ('murr_editor_image',)
