from enum import Enum

from django.db import models
from django_enum_choices.fields import EnumChoiceField


class NotificationType(Enum):
    FEED = 'feed'
    SUBSCRIBE = 'subscribe'


class BaseNotification(models.Model):
    sent_time = models.DateTimeField(
        'Время отправки', auto_now=True)
    to_murren = models.ForeignKey(
        'murren.Murren', on_delete=models.CASCADE, verbose_name='получатель',
        related_name='received_notifications')
    notification_type = EnumChoiceField(NotificationType, verbose_name="Тип уведомления")

    class Meta:
        verbose_name = 'уведомление'
        verbose_name_plural = 'уведомления'


class SubscribeNotification(BaseNotification):
    from_murren = models.ForeignKey(
        'murren.Murren', on_delete=models.CASCADE, verbose_name='Отправитель')

    class Meta:
        verbose_name = 'уведомление'
        verbose_name_plural = 'уведомления связанные с подпиской'

    def __str__(self):
        return f'{self.from_murren.username} подписался на {self.to_murren.username}'

    def save(self, *args, **kwargs):
        self.notification_type = NotificationType.SUBSCRIBE
        super().save(*args, **kwargs)


class FeedNotification(BaseNotification):
    murr_card = models.ForeignKey(
        'murr_card.MurrCard', on_delete=models.CASCADE, verbose_name='Мурр')

    class Meta:
        verbose_name = 'уведомление'
        verbose_name_plural = 'уведомления связанные c новостной лентой'

    def __str__(self):
        return f'у {self.murr_card.owner.username} новый мурр для {self.to_murren.username}'

    def save(self, *args, **kwargs):
        self.notification_type = NotificationType.FEED
        super().save(*args, **kwargs)
