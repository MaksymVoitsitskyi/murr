from allauth.socialaccount.helpers import complete_social_login
from allauth.socialaccount.providers.oauth2.client import OAuth2Error
from django.conf import settings
from djoser.serializers import UserCreateSerializer
from requests.exceptions import HTTPError
from rest_auth.registration.serializers import SocialLoginSerializer
from rest_framework import serializers

from murren.models import Murren, TrustedRegistrationEmail


class MurrenSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    subscribers_count = serializers.ReadOnlyField()
    is_subscriber = serializers.SerializerMethodField()
    likes_count = serializers.ReadOnlyField()
    dislikes_count = serializers.ReadOnlyField()

    class Meta:
        model = Murren
        fields = (
            'id',
            'username',
            'email',
            'murren_avatar',
            'avatar',
            'description',
            'subscribers_count',
            'is_subscriber',
            'likes_count', 
            'dislikes_count',
            'show_murr_card_notifications',
            'show_subscription_notifications',
        )
        read_only_fields = ('avatar', 'subscribers_count', 'is_subscriber', 'likes_count', 'dislikes_count')

    @property
    def user(self):
        request = self.context.get('request')
        return request.user if request else None

    def get_avatar(self, obj):
        return f'{settings.BACKEND_URL}{obj.murren_avatar.url}'

    def get_is_subscriber(self, obj):
        if self.user:
            if self.user.is_authenticated:
                return obj.in_subscribers(self.user)
        return False


class CustomUserCreateSerializer(UserCreateSerializer):

    def validate(self, attrs):
        user_email_service = attrs.get("email").split('@')[1]
        email_service_is_trusted = TrustedRegistrationEmail.objects.filter(service_domain=user_email_service).exists()
        if not email_service_is_trusted:
            raise serializers.ValidationError(
                {"email": ["Your email service is not allowed for register"]}
            )
        return super().validate(attrs)


class VKOAuth2Serializer(SocialLoginSerializer):
    user_id = serializers.CharField(required=False, allow_blank=True)
    callback_url = serializers.CharField(required=False, allow_blank=True)

    def validate(self, attrs):
        view = self.context.get('view')
        request = self._get_request()

        if not view:
            raise serializers.ValidationError("View is not defined, pass it as a context variable")

        adapter_class = getattr(view, 'adapter_class', None)
        if not adapter_class:
            raise serializers.ValidationError("Define adapter_class in view")

        adapter = adapter_class(request)
        app = adapter.get_provider().get_app(request)

        if attrs.get('code'):
            self.callback_url = attrs.get('callback_url')
            self.client_class = getattr(view, 'client_class', None)

            if not self.callback_url:
                raise serializers.ValidationError("Incorrect input. callback_url is required.")
            if not self.client_class:
                raise serializers.ValidationError("Define client_class in view")

            code = attrs.get('code')

            provider = adapter.get_provider()
            scope = provider.get_scope(request)
            client = self.client_class(
                request,
                app.client_id,
                app.secret,
                adapter.access_token_method,
                adapter.access_token_url,
                self.callback_url,
                scope
            )
            try:
                access_data = client.get_access_token(code)
                email = f"vk_user_{access_data['user_id']}@yandex.ru"
                access_data['email'] = email
            except OAuth2Error:
                raise serializers.ValidationError("Incorrect input. code or callback is incorrect")
        else:
            raise serializers.ValidationError("Incorrect input. code is required.")

        social_token = adapter.parse_token({'access_token': access_data['access_token']})
        social_token.app = app

        try:
            login = self.get_social_login(adapter, app, social_token, access_data)
            complete_social_login(request, login)
        except HTTPError:
            raise serializers.ValidationError('Incorrect value')

        if not login.is_existing:
            login.lookup()
            login.save(request, connect=True)
        attrs['user'] = login.account.user

        return attrs
