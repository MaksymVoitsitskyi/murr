import axios from "axios";
import {
  STATUS_200_OK,
  STATUS_204_NO_CONTENT,
  STATUS_401_UNAUTHORIZED,
} from "../../../utils/http_response_status.js";

import * as type from "./type.js";
import {
  fetchComments,
  addComment,
  likeComment,
  unlikeComment,
} from "./api.js";
import { handlerErrorAxios } from "../../../utils/helpres.js";

const COMMENTS_START_PAGE = 1;

export default {
  async [type.ACTIONS_FETCH_COMMENTS]({ commit, dispatch }, { murrId }) {
    const { success, status, data } = await fetchComments(
      murrId,
      COMMENTS_START_PAGE
    );

    if (success) {
      commit(type.MUTATIONS_SET_COMMENT, data.results);

      return data;
    }

    if (status !== STATUS_200_OK) {
      dispatch("popUpMessage", {
        message: "Произошла ошибка при загрузке комментариев!",
        customClass: "element-ui-message__error",
        type: "error",
      });
    }

    return null;
  },
  async [type.ACTIONS_FETCH_NEXT_PAGE_COMMENTS](
    { commit, dispatch },
    { murrId, page }
  ) {
    const { success, status, data } = await fetchComments(murrId, page);

    if (success) {
      commit(type.MUTATIONS_APPEND_COMMENT, data.results);

      return data;
    }

    if (status !== STATUS_200_OK) {
      dispatch("popUpMessage", {
        message: "Произошла ошибка при загрузке комментариев!",
        customClass: "element-ui-message__error",
        type: "error",
      });
    }

    return null;
  },
  async [type.ACTIONS_ADD_COMMENT]({ commit, getters, dispatch }, payload) {
    let formData = {
      author: getters.currentMurren.id,
      recaptchaToken: payload.recaptchaToken,
      murr: payload.murrId,
      text: payload.text,
    };

    const { success, status, data } = await addComment(formData);

    if (success) {
      commit(type.MUTATIONS_ADD_COMMENT, data);
      return data;
    }

    if (status === STATUS_401_UNAUTHORIZED) {
      dispatch("popUpMessage", {
        message: "Для создания комментариев требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
    }

    return null;
  },
  async [type.ACTIONS_REPLY_COMMENT]({ dispatch, getters }, payload) {
    let formData = {
      author: getters.currentMurren.id,
      recaptchaToken: payload.recaptchaToken,
      murr: payload.murrId,
      text: payload.text,
      parent: payload.parent,
    };
    const { success, status, data } = await addComment(formData);

    if (success) {
      return data;
    }

    if (status === STATUS_401_UNAUTHORIZED) {
      dispatch("popUpMessage", {
        message: "Для создания комментариев требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
    }

    return null;
  },
  async [type.ACTIONS_LIKE]({ dispatch }, payload) {
    const { success, status, data } = await likeComment(payload.commentId);

    if (success) {
      return data;
    }

    if (status === STATUS_401_UNAUTHORIZED) {
      dispatch("popUpMessage", {
        message: "Требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
    }

    return null;
  },
  async [type.ACTIONS_UNLIKE]({ dispatch }, payload) {
    const { success, status, data } = await unlikeComment(payload.commentId);

    if (success) {
      return data;
    }

    if (status === STATUS_401_UNAUTHORIZED) {
      dispatch("popUpMessage", {
        message: "Требуется авторизация 😳",
        customClass: "element-ui-message__warning",
        type: "warning",
      });
    }

    return null;
  },
  async [type.ACTIONS_DELETE_COMMENT](_, { commentId }) {
    try {
      const { data, status } = await axios.delete(
        `/api/murr_comments/${commentId}/`
      );

      if (status === STATUS_204_NO_CONTENT) {
        return { success: true, message: "Коммент успешно удален!" };
      }

      return { success: false, message: data.detail };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },
};
