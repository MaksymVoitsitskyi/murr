import axios from "axios";

import { STATUS_204_NO_CONTENT } from "../../../utils/http_response_status.js";
import { handlerErrorAxios } from "../../../utils/helpres.js";
import * as type from "./type.js";

export default {
  async [type.READ_MURR_CARD](_, murrId) {
    try {
      const { status } = await axios.delete(
        `/api/notifications/${murrId}/read_murr_card/`
      );

      if (status === STATUS_204_NO_CONTENT) {
        return {
          success: true,
        };
      }

      return { success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },

  async [type.READ_SUBSCRIBE_NOTIFY](_, notifyId) {
    try {
      const { status } = await axios.delete(
        `/api/notifications/${notifyId}/read_subscribe_notify/`
      );

      if (status === STATUS_204_NO_CONTENT) {
        return {
          success: true,
        };
      }

      return { success: false };
    } catch (error) {
      return handlerErrorAxios(error);
    }
  },
};
