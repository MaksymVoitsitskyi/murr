import actions from "./actions.js";
import mutations from "./mutations.js";

export default {
  state: {
    murrCardNotices: [],
  },
  getters: {
    hasNewMurr: (state) => !!state.murrCardNotices.length,
    murrCardNotices: (state) => state.murrCardNotices,
  },
  actions,
  mutations,
};
