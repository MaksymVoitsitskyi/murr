import axios from "axios";
import { STATUS_200_OK } from "../../utils/http_response_status.js";
import { handlerErrorAxios } from "../../utils/helpres.js";

export default {
  state: {
    currentMurren: null,
  },
  actions: {
    async createToken({ commit, dispatch }, payload) {
      try {
        const tokenData = await axios.post(
          "/api/murren/token_create/",
          payload
        );
        if (tokenData.data.token) {
          axios.defaults.headers[
            "Authorization"
          ] = `Bearer ${tokenData.data.token}`;

          commit("setCurrentMurren", { token: tokenData.data.token });
          dispatch("fetchCurrentMurren");
        }
      } catch (e) {
        return {
          error: true,
          accountActivated: !(
            e.response.data.non_field_errors[0] ===
            "Unable to log in with provided credentials."
          ),
        };
      }

      return {
        error: false,
        accountActivated: true,
      };
    },
    async createMurren(_, payload) {
      let results = {
        murrenIsCreated: false,
        uniqueName: false,
        uniqueEmail: false,
        passwordIsTooCommon: false,
        recaptchaError: false,
        passwordIsTooSimilarToUsername: false,
        passwordIsTooSimilarToEmail: false,
      };
      try {
        const { status } = await axios.post("/auth/users/", payload);

        if (status === 201) {
          results.murrenIsCreated = true;
        }

        return results;
      } catch (e) {
        if (
          e.response.data.username &&
          e.response.data.username[0] ===
            "A user with that username already exists."
        ) {
          results.uniqueName = true;
        }

        if (
          e.response.data.email &&
          e.response.data.email[0] === "user with this email already exists."
        ) {
          results.uniqueEmail = true;
        }

        if (
          e.response.data.password &&
          e.response.data.password[0] === "This password is too common."
        ) {
          results.passwordIsTooCommon = true;
        }

        if (
          e.response.data.password &&
          e.response.data.password[0] ===
            "The password is too similar to the username."
        ) {
          results.passwordIsTooSimilarToUsername = true;
        }

        if (
          e.response.data.password &&
          e.response.data.password[0] ===
            "The password is too similar to the email."
        ) {
          results.passwordIsTooSimilarToEmail = true;
        }

        if (e.response.data.recaptcha_response_problem) {
          results.recaptchaError = true;
        }

        return results;
      }
    },
    async requestResetPassword(_, payload) {
      try {
        let results = {
          emailIsSent: false,
          notFoundMurren: false,
        };

        const { status } = await axios.post(
          "/auth/users/reset_password/",
          payload
        );

        if (status === 204) {
          results.emailIsSent = true;
        }

        return results;
      } catch (e) {
        return { error: true, message: "Ошибка на сервере" };
      }
    },
    async setNewPassword(_, payload) {
      let results = {
        passwordIsChanged: false,
        passwordIsTooCommon: false,
      };

      try {
        const data = await axios.post("/auth/users/reset_password_confirm/", {
          uid: payload.uid,
          token: payload.token,
          new_password: payload.password,
          re_new_password: payload.passwordRepeat,
          recaptchaToken: payload.recaptchaToken,
        });

        if (data.status === 204) {
          results.passwordIsChanged = true;
        }

        return results;
      } catch (e) {
        if (
          e.response.data.new_password &&
          e.response.data.new_password[0] === "This password is too common."
        ) {
          results.passwordIsTooCommon = true;
          return results;
        }
        return { error: true, message: "Ошибка на сервере" };
      }
    },
    async mailConfirmation(_, payload) {
      try {
        let results = {
          isActivated: false,
          otherError: false,
        };

        const data = await axios.post("/auth/users/activation/", {
          uid: payload.uid,
          token: payload.token,
        });

        if (data.status === 204) {
          results.isActivated = true;
        }

        return results;
      } catch (e) {
        return { error: true, message: "Ошибка на сервере" };
      }
    },
    async vkOauthLogin({ commit }, payload) {
      try {
        const { data, status } = await axios.post("/api/murren/oauth/vk/", {
          code: payload.code,
          callback_url: payload.callback_url,
        });

        if (status === STATUS_200_OK) {
          axios.defaults.headers["Authorization"] = `Bearer ${data.token}`;
          const profileData = await axios.get("/api/murren/profile/");
          let profile = {
            ...profileData.data,
            token: data.token,
          };
          commit("setCurrentMurren", profile);

          return { success: true, data };
        }

        return { success: false, data };
      } catch (error) {
        return handlerErrorAxios(error);
      }
    },
    async facebookOauthLogin({ commit }, payload) {
      try {
        const { data, status } = await axios.post(
          "/api/murren/oauth/facebook/",
          {
            access_token: payload.token,
          }
        );

        if (status === STATUS_200_OK) {
          axios.defaults.headers["Authorization"] = `Bearer ${data.token}`;
          const profileData = await axios.get("/api/murren/profile/");
          let profile = {
            ...profileData.data,
            token: data.token,
          };
          commit("setCurrentMurren", profile);

          return { success: true, data };
        }

        return { success: false, data };
      } catch (error) {
        return handlerErrorAxios(error);
      }
    },
    async logout({ commit }) {
      commit("logout_mutations");
    },
    async fetchCurrentMurren({ commit }) {
      const { data } = await axios.get("/api/murren/profile/");
      commit("setCurrentMurren", data);
    },
  },
  mutations: {
    logout_mutations(state) {
      delete axios.defaults.headers["Authorization"];
      state.currentMurren = null;
    },
    setCurrentMurren(state, murrenData) {
      if (state.currentMurren) {
        state.currentMurren = { ...state.currentMurren, ...murrenData };
      } else {
        state.currentMurren = murrenData;
      }
    },
    updateAvatar(state, { avatar }) {
      state.currentMurren = {
        ...state.currentMurren,
        avatar: avatar,
        murren_avatar: avatar,
      };
    },
  },
  getters: {
    currentMurren(state) {
      return state.currentMurren;
    },
  },
};
